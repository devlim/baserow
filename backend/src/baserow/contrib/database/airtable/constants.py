AIRTABLE_EXPORT_JOB_DOWNLOADING_BASE = "downloading-base"
AIRTABLE_EXPORT_JOB_CONVERTING = "converting"
AIRTABLE_EXPORT_JOB_DOWNLOADING_FILES = "downloading-files"
AIRTABLE_BASEROW_COLOR_MAPPING = {
    "blue": "blue",
    "cyan": "light-blue",
    "teal": "light-green",
    "green": "green",
    "yellow": "light-orange",
    "orange": "orange",
    "red": "light-red",
    "pink": "red",
    "purple": "dark-blue",
    "gray": "light-gray",
}
AIRTABLE_NUMBER_FIELD_SEPARATOR_FORMAT_MAPPING = {
    "commaPeriod": "COMMA_PERIOD",
    "periodComma": "PERIOD_COMMA",
    "spaceComma": "SPACE_COMMA",
    "spacePeriod": "SPACE_PERIOD",
}
